# my tiny python package

this is my tiny python package, you can contribuate

## consignes TP:

1. cloner le répértoire git : git clone 
2. naviguer vers le répo git 
3. faire un checkout de la branche develop : git checkout develop
4. créer une branche feature/votre-nom : git branch feature/...
5. naviguer vers votre branche : git checkout
6. créer un dossier portant votre nom dans lequel il y a un fichier qui porte votre nom
7. dans la racine du projet créer un fichier "members_list.txt" dans lequel vous ajoutez une ligne qui porte votre nom
8. Ajouter un commit
9. Poussez la branche vers le dépot distant : git push
10. creer une nouvelle branche feature/votre_nom-security
11. merger la branche feature/nom-security dans la branche feature/nom
12. provoquer un conflit dans un fichier en ajoutant dans deux branches distinctes la même ligne dans le même fichier portant une valeure differente / 
13. gerer le conflit lors du merge
14. Créer une merge request de la branche feature/nom vers develop

// gestion de conflits gitflow
dans le repo local faire un merge de develop vers feature/.. : git checkout feature/... , git pull, git merge origin/develop
gerer les conflits localement
pousser les changement : git push
dans gitlab, ouvrir une merge request de votre branche vers develop


## ajouter un remote
 * git remote add origin "url-depot-distant-privé / fork"
 * git remote add upstream "url-depot-distant-origin"

## renommer un remote
 * git remote rename origin old-origin
 * git remote add origin "url-depot-distant-privé / fork"

## verifier les urls des remotes
* git config --get remote.origin.url
* git config --get remote.upstream.url 
## Contribute !

You can contribute to my project

## add changes

You can add whatever you want

# 1 git pull upsteam master 

# 2 effectuer des changement 

# 3 add a commmit : git add . ; git commit -m "message"

# 4 git push origin master 

# 5 merge request depuis gitlab

